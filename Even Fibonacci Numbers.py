__author__ = 'Jason'


def main():

    def fib(i):
        numbers = [0, 1, 2]
        numbers_length = len(numbers)
        total = 0
        while numbers_length < (i + 1):
            second_to_last_number = numbers[numbers_length - 2]
            last_number = numbers[numbers_length - 1]
            last_fib = last_number + second_to_last_number
            numbers.append(last_fib)
            if last_fib % 2 == 0:
                total += last_fib
                if (total + 2) >= 4000000:
                    break
            numbers_length += 1
        print("Sum: ", total + 2)

    fib(100)


main()