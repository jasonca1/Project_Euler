__author__ = 'Jason'

for n in range(1, 1000):
    if n % 3 == 0 and n % 5 == 0:
        print(n)

total = sum([x for x in range(1, 1000) if not x % 3 or not x % 5])
print(total)