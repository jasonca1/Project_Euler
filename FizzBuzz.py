__author__ = 'Jason'
# FizzBuzz
for n in range(1, 101):
    if n % 15 == 0:
        print(n, " :FizzBuzz")
    if n % 3 == 0 and n % 15 != 0:
        print(n, " :Fizz")
    if n == 5 and n % 15 != 0:
        print(n, " :Buzz")
    else:
        print(n)